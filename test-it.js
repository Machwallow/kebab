const fs = require('fs') 

module.exports = {
  getFunctionFrom: (file) => {
    let source = fs.readFileSync(file)

    let f = new Function('params', `
      let lambda = ${source}      
      return lambda(params)
    `)
    return f
  }, 
  assert: (what, something) => {
    if(something) {
        console.log("??", what, "test is ?")
    } else {
        console.log("??", what, "test is ??")
        process.exit(1)
    }
  },
  end: () => process.exit(0)
}
